import Phaser from 'phaser'

export default class extends Phaser.State {
  init () {}

  preload () {
    let text = this.add.text(
      this.world.centerX,
      this.world.centerY,
      'Game Over',
      {
        font: '64px Arial',
        fill: '#dddddd',
        align: 'center'
      }
    )
    text.anchor.setTo(0.5, 0.5)
  }

  render () {}
}
