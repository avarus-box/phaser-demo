import Phaser from 'phaser'
import config from '../config'
import utils, { scaleSprite, destroySpirtes, scaleSpriteXY } from '../utils'
import IntroBg from '../sprites/IntroBg'
import IntroElement from '../sprites/IntroElement'

import NormalText from '../sprites/NormalText'

export default class extends Phaser.State {
  init () {
    this.textScaleFactor = 0.001
    this.startButtonSound = null
    this.scale.scaleMode = Phaser.ScaleManager.RESIZE
    this.scale.refresh()
  }

  preload () {
    // this.loaderBar = this.add.sprite(
    //   this.game.world.centerX,
    //   this.game.world.centerY,
    //   'loaderBar'
    // )
    // centerGameObjects([this.loaderBg, this.loaderBar])
    // this.load.setPreloadSprite(this.loaderBar)

    this.load.image('street', './assets/images/street-bg.png')
    this.load.image('introBg', './assets/images/game_intro.png')
    this.load.image('gameTitle', './assets/images/game_title.png')
    this.load.image('startGameBtn', './assets/images/start_button.png')
    this.load.image('introBoy', './assets/images/game_intro_character.png')
  }

  create () {
    this.game.bgmusic = this.game.add.audio('music')
    this.game.bgmusic.play()

    this.startButtonSound = this.add.audio('startButtonSound')

    // Add Background
    this.back = this.add.image(0, 0, 'street')
    this.back.scale.set(this.game.height / config.gameHeight)
    this.back.smoothed = false
    // Add Intro
    this.addIntro()
  }

  // onResize
  resize (width, height) {
    this.back.scale.set(height / config.gameHeight)

    this.introBg.resize(width, height)
    scaleSprite(this.introBg, this.game.width, this.game.height, 0, 0.95)

    this.gameTitle.resize(width, height)
    scaleSprite(this.gameTitle, this.game.width, this.game.height, 0, 0.32)

    this.startGameBtn.resize(width, height)
    scaleSprite(this.startGameBtn, this.game.width, this.game.height, 20, 0.15)

    this.gameBoy.resize(width, height)
    scaleSprite(this.gameBoy, this.game.width, this.game.height, 20, 0.35)

    this.detailsText.resize(this.world.width, this.world.height)
    this.scale.refresh()
  }

  addIntro () {
    // introBg
    this.introBg = new IntroBg({
      game: this.game,
      x: this.world.width * 0.5,
      y: this.world.height * 0.5,
      asset: 'introBg'
    })
    // this.introBg.scale.set(0.21)
    this.introBg.anchor.setTo(0.5)
    this.introBg.smoothed = false
    this.introBg.resize = (width, height) => {
      this.introBg.x = this.world.centerX
      this.introBg.y = this.world.centerY
    }
    scaleSprite(this.introBg, this.game.width, this.game.height, 0, 0.95)
    this.add.existing(this.introBg)

    // gameTitle
    this.gameTitle = new IntroElement({
      game: this.game,
      x: this.world.width * 0.5,
      y: this.world.height * 0.35,
      // relativeX: 0.5,
      // relativeY:  0.32,
      asset: 'gameTitle'
    })
    this.gameTitle.resize = (width, height) => {
      this.gameTitle.x = this.world.centerX
      this.gameTitle.y = this.introBg.y - this.introBg.height * 0.21
    }
    this.gameTitle.resize(this.world.width, this.world.height)
    scaleSprite(this.gameTitle, this.game.width, this.game.height, 0, 0.32)
    this.add.existing(this.gameTitle)

    // startGameBtn
    this.startGameBtn = new IntroElement({
      game: this.game,
      x: this.world.width * 0.5,
      y: this.world.height * 0.7,
      asset: 'startGameBtn'
    })
    this.startGameBtn.resize = (width, height) => {
      this.startGameBtn.x = this.world.centerX
      this.startGameBtn.y = this.introBg.y + this.introBg.height * 0.28
    }
    this.startGameBtn.resize(this.world.width, this.world.height)
    scaleSprite(this.startGameBtn, this.game.width, this.game.height, 20, 0.15)
    this.startGameBtn.inputEnabled = true
    this.startGameBtn.input.useHandCursor = true
    this.startGameBtn.events.onInputDown.add(() => {
      this.startButtonSound.play()
      this.state.start('Game')
    }, this)

    this.add.existing(this.startGameBtn)
    // this.currentSpirtes.push(this.startGameBtn)

    // Game Boy
    this.gameBoy = new IntroElement({
      game: this.game,
      x: this.world.width * 0.4,
      y: this.world.height * 0.9,
      asset: 'introBoy'
    })
    this.gameBoy.resize = (width, height) => {
      this.gameBoy.x = this.world.width * 0.4
      this.gameBoy.y = this.world.height * 0.9
    }
    this.gameBoy.resize(this.world.width, this.world.height)
    scaleSprite(this.gameBoy, this.game.width, this.game.height, 20, 0.35)
    this.add.existing(this.gameBoy)

    this.detailsText = new NormalText({
      game: this.game,
      text:
`請注意，呢個平台所提供嘅資料，係參考
資歷架構下第二級別(QF2)嘅課程，大部分
課程入學資格為讀完中三。

課程資料不時更新，如果想知多啲，建議你
睇返資歷名冊。`
    })
    this.detailsText.addColor('#0072C6', this.detailsText.text.length - 5) // position 尾5開始用紅字
    this.detailsText.addColor('black', this.detailsText.text.length - 1) // position 尾1用黑字
    this.detailsText.style.align = 'left'
   
    // Add resize() methods
    this.detailsText.anchor.setTo(0.5)
    this.detailsText.resize = (width, height) => {
      const scale = this.introBg.height * 1.2 * this.textScaleFactor
      this.detailsText.scale.x = scale
      this.detailsText.scale.y = scale

      this.detailsText.x = this.world.centerX
      this.detailsText.y = this.world.centerY + this.introBg.height * 0.025
    }
    this.detailsText.inputEnabled = true
    this.detailsText.input.useHandCursor = true
    this.detailsText.events.onInputDown.add(() => {
      window.open(
        'http://www.hkqr.gov.hk/HKQRPRD/web/hkqr-tc/search/qr-search/',
        '_blank'
      )
    })
    this.detailsText.resize(this.world.width, this.world.height)
    this.add.existing(this.detailsText)
    // this.currentSpirtes.push(detailsText)
  }
}
