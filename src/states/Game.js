/* globals __DEV__ */
import Phaser from 'phaser'
import axios from 'axios'
// import Mushroom from '../sprites/Mushroom'
import config from '../config'
import utils, { scaleSprite, destroySpirtes, scaleSpriteXY } from '../utils'
import TitleText from '../sprites/TitleText'
import OptionText from '../sprites/OptionText'
import OptionImage from '../sprites/OptionImage'
import PageText from '../sprites/PageText'
import NormalText from '../sprites/NormalText'
import Boy from '../sprites/Boy'
import CourseImage from '../sprites/CourseImage'
import Box from '../sprites/Box'
import Icon from '../sprites/Icon'
import IntroBg from '../sprites/IntroBg'
import IntroElement from '../sprites/IntroElement'

Promise = require('bluebird')

export default class extends Phaser.State {
  init () {
    this.back = null
    this.boy = null
    this.box = null
    this.boyWalkTime = 2000 // ms
    this.boyAnim = null
    this.currentSpirtes = []
    this.selectedCourse = null
    this.selectedProg = null
    this.courseImage = null
    this.charachterName = null
    this.curStep = 0
    this.lastStep = 4
    this.bgmusic = null
    this.musicIcon = null
    this.buttonClickSound = null
    this.characterSound = null
    this.introBg = null
    this.gameTitle = null
    this.gameCharacter = null
    this.startGameBtn = null
    // this.mouseOver = null

    this.textScaleFactor = 0.001

    this.data = {}

    this.questions = [
      {
        qId: 1,
        qtext: '',
        options: []
      },
      {
        qId: 2,
        qtext: '平時你最鍾意?',
        options: [
          { id: 101, text: '整嘢食' },
          { id: 102, text: '做運動' },
          { id: 103, text: '畫嘢' },
          { id: 104, text: '影相' },
          { id: 105, text: '扮靚' },
          { id: 106, text: '打機' },
          { id: 107, text: '唱歌' },
          { id: 108, text: '去旅行' }
        ]
      },
      {
        qId: 3,
        qtext: '呢個可能啱你',
        options: [
          { id: 201, text: 'Options 201' },
          { id: 202, text: 'Options 202' }
        ]
      },
      {
        qId: 4,
        qtext: '話唔定你會變咗另一個',
        options: [
          { id: 301, text: 'Options 301' },
          { id: 302, text: 'Options 302' }
        ]
      }
    ]
  }

  preload () {
    // this.load.image('introBg', './assets/images/game_intro.png')
    // this.load.image('gameTitle', './assets/images/game_title.png')
    // this.load.image('gameCharacter', './assets/images/game-character.png')
    this.load.image('startGameBtn', './assets/images/start_button.png')
    this.load.image('street', './assets/images/street-bg.png')
    // this.load.image('startButton', './assets/images/enter_button.png')
    // this.load.image('stopButton', './assets/images/stop_button.png')
    this.load.image('mute', './assets/images/mute.png')
    this.load.image('sound', './assets/images/sound.png')
    this.load.image('box', './assets/images/box.png')
    this.load.image('facebookShare', './assets/images/facebook_s.png')
    this.load.image('againButton', './assets/images/againButton.png')
    this.load.spritesheet(
      'people',
      './assets/images/newboy-01.png',
      137.5,
      271,
      10
    )
  }

  create () {
    // alert(
    //   " window.innerWidth "+ window.innerWidth + " window.innerHeight " + window.innerHeight +
    //   " window.outerWidth "+ window.outerWidth + " window.outerHeight " + window.outerHeight +
    //   " window.screen.width "+ window.screen.width + " window.screen.height " + window.screen.height +
    //   " window.screen.availWidth "+ window.screen.availWidth + " window.screen.availHeight " + window.screen.availHeight +
    //   " document.documentElement.clientWidth " + document.documentElement.clientWidth +
    //   " document.documentElement.clientHeight " + document.documentElement.clientHeight
    // )
    // Let bgmGroup = this.add.group()
    // Add Background
    this.back = this.add.image(0, 0, 'street')
    this.back.scale.set(this.game.height / config.gameHeight)
    this.back.smoothed = false
    // let gameTitle = this.add.image(this.game.width * 0.35, this.game.height * 0.25, 'gameTitle')
    // gameTitle.scale.set(this.game.height / config.gameHeight)
    // this.displayIntro()

    // Add Boy
    this.boy = new Boy({
      game: this.game,
      x: this.world.width * 0.2,
      y: this.world.height * 0.6,
      relativeX: 0.2,
      relativeY: 0.5,
      asset: 'people'
    })
    this.add.existing(this.boy)
    this.boy.resize(this.world.width, this.world.height)
    scaleSprite(this.boy, this.game.width, this.game.height / 3, 0, 1.5)

    // Add Boy Animation
    this.boyAnim = this.boy.animations.add('walk')
    this.boyAnim.onStart.add(this.boyStartWalk, this)
    this.boyAnim.onLoop.add(this.boyWalking, this)
    this.boyAnim.onComplete.add(this.boyCompleteWalk, this)

    // Add mute icon
    this.addMuteIcon()

    // Add background music
    this.bgmusic = this.game.bgmusic
    this.buttonClickSound = this.add.audio('buttonClickSound')
    this.pageClickSound = this.add.audio('pageClickSound')
    this.characterSound = this.add.audio('characterSound')
    // this.mouseOver = this.add.audio('mouseOver')

    //this.bgmusic.play()
    this.startGame()
  }

  update () {
    if (this.boyAnim.isPlaying) {
      this.back.x -= 5
    }
  }

  render () {
    if (__DEV__) {
      // this.game.debug.spriteInfo(this.mushroom, 32, 32)
    }
  }

  addMuteIcon () {
    // Add mute icon
    this.musicIcon = new Icon({
      game: this.game,
      x: this.world.width * 0.88,
      y: this.world.height * 0.02,
      relativeX: 0.88,
      relativeY: 0.02,
      asset: 'mute'
    })
    this.musicIcon.input.useHandCursor = true
    this.musicIcon.events.onInputDown.add(() => {
      this.bgmusic.mute = true
      this.musicIcon.destroy(true)
      this.addSoundIcon()
    }, this)

    this.add.existing(this.musicIcon)
    scaleSprite(this.musicIcon, this.game.width, this.game.height / 3, 0, 0.2)
  }

  addSoundIcon () {
    // Add mute icon
    this.musicIcon = new Icon({
      game: this.game,
      x: this.world.width * 0.88,
      y: this.world.height * 0.02,
      relativeX: 0.88,
      relativeY: 0.02,
      asset: 'sound'
    })
    this.musicIcon.input.useHandCursor = true
    this.musicIcon.events.onInputDown.add(() => {
      this.bgmusic.mute = false
      this.musicIcon.destroy(true)
      this.addMuteIcon()
    }, this)
    this.add.existing(this.musicIcon)
    scaleSprite(this.musicIcon, this.game.width, this.game.height / 3, 0, 0.2)
  }

  // onResize
  resize (width, height) {
    // Resize Background
    this.back.scale.set(height / config.gameHeight)

    // Resize Sprites
    this.boy.resize(width, height)
    scaleSprite(this.boy, width, height / 3, 0, 1.5)

    this.musicIcon.resize(width, height)
    scaleSprite(this.musicIcon, width, height / 3, 0, 0.2)
    // this.back.height = height
    // this.back.width = width

    // Resize Current Sprites
    this.currentSpirtes.forEach(sprite => {
      if (utils.isGroup(sprite)) {
      } else {
        if (sprite.resize !== undefined) {
          // Instance's resize handler
          sprite.resize(width, height)

          // Scale
          if (utils.isText(sprite)) {
            // Scale Text
            this.add.existing(sprite)
          } else if (sprite.scaleX !== undefined) {
            // Scale Sprite
            scaleSpriteXY(
              sprite,
              width,
              height / 3,
              50,
              sprite.scaleX,
              sprite.scaleY
            )
            // console.log('scale resize: ' + sprite.scaleX)
          } else {
            scaleSprite(sprite, width, height / 3, 50, 1)
          }
        } else {
          let oldScaleX = sprite.scale.x
          let oldScaleY = sprite.scale.y
          scaleSprite(sprite, width, height / 3, 50, 1)
          sprite.x = sprite.x * sprite.scale.x / oldScaleX
          sprite.y = sprite.y * sprite.scale.y / oldScaleY
          //   // this.destroy(this.currentSpirte[i]);
        }
      }
    })
    this.scale.refresh()
  }

  async startGame () {
    try {
      let response = await axios.get('./data.json')
      this.data = response.data
      // console.log(this.data)
    } catch (error) {
      alert('Cannot load data')
      // console.log('Cannot load Data', error)
    }

    this.curStep = 1

    // Play - frameRate:10, loop: true
    this.boyAnim.play(10, true)
    this.time.events.add(
      this.boyWalkTime,
      () => {
        // console.log('Boy stopped walk after 5 seconds')
        this.boyAnim.loop = false
      },
      this
    )
  }

  boyStartWalk (sprite, animation) {}
  boyWalking (sprite, animation) {
    if (animation.loopCount === 1) {
    }
  }
  boyCompleteWalk (sprite, animation) {
    if (this.curStep < this.lastStep) {
      this.displayStep(this.curStep)
    }
  }

  walkToNextStep () {
    // Destroy Current Sprites
    destroySpirtes(this.currentSpirtes)
    this.currentSpirtes = []

    this.curStep += 1

    if (this.curStep > this.lastStep) {
      // Walk to Edge and Change State
      this.boyAnim.play(10, true)
      // Tween (Effect) x: this.world.width, speed: 1200 (ms)
      let tween = this.add
        .tween(this.boy)
        .to({ x: this.world.width }, 2400, Phaser.Easing.Linear.In, true)
      tween.onComplete.add(() => {
        this.state.start('GameOver')
      }, this)
    } else {
      // Walk X seconds
      this.boyAnim.play(10, true)
      this.time.events.add(
        this.boyWalkTime,
        () => {
          console.log('Boy stopped walk after 5 seconds')
          this.boyAnim.loop = false
        },
        this
      )
    }
  }

  addHobbyOptions () {
    let options = this.data.hobbies

    // Create OptionText(s)
    for (let i = 0; i < options.length; i++) {
      let opt = options[i]

      // Create OptionText
      let optionText = new OptionImage({
        game: this.game,
        // x: this.world.width * 0.6,
        // y: this.world.y + 120 + i * 40,
        asset: opt.hobbiesImage
      })
        optionText.resize = (width, height) => {
            // Scale
            const scale = height * 0.99 * this.textScaleFactor
            optionText.scale.x = scale
            optionText.scale.y = scale
            let factorX = i
            // if (i <= 7) {
            factorX = this.world.centerX +
              width * 0.1 -
              width * 0.19 +
              Math.floor(i % 4) * width * 0.11
            // Position
          
            let factorY = i
            if (i > 2 & i <= 5) {
              factorY = i - 3
            } else if (i > 5 & i <= 8) {
              factorY = i - 6
            } else if (i > 8 & i <= 11) {
              factorY = i - 9
            } else if (i > 11) {
              factorY = i - 10
            }
            
            optionText.x = factorX
            optionText.y = height * 0.17 + factorY * (optionText.height + 11)
            console.log(`${opt.text} : ${optionText.x} , ${optionText.y}`)
          }
        optionText.scaleX = 0.68
        optionText.scaleY = 0.68
        optionText.resize(this.world.width, this.world.height)
        scaleSprite(optionText, this.game.width / 3, this.game.height / 3, 50, 0.68)

      // Add events
      optionText.events.onInputDown.add(() => {
        if (this.bgmusic.mute === false) {
          this.buttonClickSound.play()
        }
        optionText.fill = 'red'
        console.log(`click ${opt.text}`)
        this.characterName = opt.name
        this.selectedCourse = opt
        this.walkToNextStep()
      }, this)

      // optionText.events.onInputOver.add(() => {
      //   this.mouseOver.play()
      // }, this)

      this.add.existing(optionText) // Add to Game
      this.currentSpirtes.push(optionText) // Save to Array
    }
  }

  addCourseOptions () {
    let selectedCourseid = this.selectedCourse.id
    let options = this.data.courses.filter(function (item) {
      if (item.hobbies_id === selectedCourseid) {
        return true
      }
      return false
    })
    let total = options.length
    let pageSize = 5
    let curPage = 1
    let totalpage = Math.ceil(total / pageSize)
    // Create Group
    let courseGroup = this.add.group()

    const showCourses = page => {
      let start = (page - 1) * pageSize
      let end = Math.min(start + pageSize, total)

      courseGroup.destroy(true)
      courseGroup = this.add.group()

      let count = -1
      for (let i = start; i < end; i++) {
        count += 1
        let opt = options[i]
        // Create OptionText
        let optionText = new OptionText({
          game: this.game,
          // x: this.world.width * 0.6,
          // y: this.world.y + 120 + i * 40,
          text: opt.text
        })
        // Add resize() methods
        optionText.offset = count
        optionText.resize = (width, height) => {
          // Scale
          const scale = height * this.textScaleFactor
          optionText.scale.x = scale
          optionText.scale.y = scale

          // Positon
          optionText.x = this.world.centerX - width * 0.10
          optionText.y = height * 0.18 + optionText.offset * (optionText.height + 7)
        }
        optionText.resize(this.world.width, this.world.height)
        // scaleSprite(optionText, this.game.width, this.game.height / 3, 50, 0.8)

        // Add events
        optionText.events.onInputDown.add(() => {
          if (this.bgmusic.mute === false) {
            this.buttonClickSound.play()
          }
          optionText.fill = 'red'
          this.selectedProg = opt
          console.log(`click ${opt.text}`)
          this.walkToNextStep()
        }, this)

        // Add to Game
        this.add.existing(optionText)
        this.currentSpirtes.push(optionText)

        // Add to Group
        courseGroup.add(optionText)
      }
      if (curPage > 1) {
        let prevBtn = new PageText({
          game: this.game,
          x: this.world.width * 0.58,
          y: this.world.height * 0.65,
          text: '上一頁'
        })
        // this.add.text(this.world.width * 0.58, this.world.height * 0.5, '上一頁')
        prevBtn.inputEnabled = true
        prevBtn.events.onInputDown.add(() => {
          this.pageClickSound.play()
          curPage -= 1
          showCourses(curPage)
        })
        prevBtn.resize = (width, height) => {
          const scale = height * 1 * this.textScaleFactor
          prevBtn.scale.x = scale
          prevBtn.scale.y = scale
    
          prevBtn.x = this.world.centerX + width * 0.1 - width * 0.1
          prevBtn.y = height * 0.65
        }
        prevBtn.resize(this.world.width, this.world.height)
        this.add.existing(prevBtn)
        this.currentSpirtes.push(prevBtn)
        courseGroup.add(prevBtn)
      }
      if (curPage < totalpage) {
        let nextBtn = new PageText({
          game: this.game,
          x: this.world.width * 0.65,
          y: this.world.height * 0.65,
          text: '下一頁'
        })
        // this.add.text(this.world.width * 0.65, this.world.height * 0.5, '下一頁')
        nextBtn.inputEnabled = true
        nextBtn.resize = (width, height) => {
          const scale = height * 1 * this.textScaleFactor
          nextBtn.scale.x = scale
          nextBtn.scale.y = scale
    
          nextBtn.x = this.world.centerX + width * 0.1 + width * 0.1
          nextBtn.y = height * 0.65
        }
        nextBtn.resize(this.world.width, this.world.height)

        nextBtn.events.onInputDown.add(() => {
          this.pageClickSound.play()
          curPage += 1
          showCourses(curPage)
        })
        this.currentSpirtes.push(nextBtn)
        courseGroup.add(nextBtn)
      }
    }
    showCourses(1)
    // // Create OptionText(s)
    // for (let i = 0; i < options.length; i++) {
    //   let opt = options[i]

    //   // Create OptionText
    //   let optionText = new OptionText({
    //     game: this.game,
    //     // x: this.world.width * 0.6,
    //     // y: this.world.y + 120 + i * 40,
    //     text: opt.text
    //   })
    //   // Add resize() methods
    //   optionText.resize = (width, height) => {
    //     optionText.x = this.world.width * 0.6
    //     optionText.y = this.world.height * 0.12 + i * optionText.height
    //   }
    //   optionText.resize()
    //   scaleSprite(optionText, this.game.width, this.game.height / 3, 50, 1)

    //   // Add events
    //   optionText.events.onInputDown.add(() => {
    //     optionText.fill = 'red'
    //     console.log(`click ${opt.text}`)
    //     this.walkToNextStep()
    //   }, this)

    //   // Add to Game
    //   this.add.existing(optionText)

    //   this.currentSpirtes.push(optionText)
    // }
  }
  async addCourseDetails () {
    // Create OptionText
    console.log(`selectedCourse ${this.selectedCourse.image}`)

    let p = new Promise((resolve, reject) => {
      this.load.image(
        'courseImage',
        './assets/images/' + this.selectedCourse.image
      )
      this.load.start()
      this.load.onLoadComplete.add(() => {
        resolve(true)
      }, this)
    })
    await p

    // Add course image
    this.courseImage = new CourseImage({
      game: this.game,
      x: this.world.width * 0.4,
      y: this.world.height * 0.32,
      relativeX: 0.4,
      relativeY: 0.32,
      asset: 'courseImage'
    })

    this.courseImage.anchor.setTo(0.5)
    this.courseImage.resize = (width, height) => {
      this.courseImage.x = this.world.centerX + width * 0.12
      this.courseImage.y = this.world.height * 0.32
    }
    this.courseImage.resize(this.world.width, this.world.height)
    scaleSprite(
      this.courseImage,
      this.game.width,
      this.game.height / 3,
      0,
      1.25
    )
    this.add.existing(this.courseImage)
    this.currentSpirtes.push(this.courseImage)

    // add name
    let character = new NormalText({
      game: this.game,
      // x: this.world.width * 0.6,
      // y: this.world.y + 120 + i * 40,
      text: this.characterName
    })
    character.inputEnabled = true
    character.anchor.setTo(0.5)
    character.resize = (width, height) => {
      const scale = height * 1 * this.textScaleFactor
      character.scale.x = scale
      character.scale.y = scale

      character.x = this.world.centerX + width * 0.2
      character.y = height * 0.3
    }
    character.resize(this.world.width, this.world.height)
    this.add.existing(character)
    // if (this.bgmusic.mute === false) {
    this.characterSound.play()
    // }
    this.currentSpirtes.push(character)

    const programName = this.selectedProg.text.trim()
    let detailsText = new NormalText({
      game: this.game,
      text: `你啱啱揀咗 
${programName}
如果想知多啲呢個課程嘅資料，可以睇返資歷名冊。`
    })
    detailsText.addColor('#0072C6', detailsText.text.length - 5) // position 尾5開始用紅字
    detailsText.addColor('black', detailsText.text.length - 1) // position 尾1用黑字
    detailsText.inputEnabled = true
    detailsText.input.useHandCursor = true
    detailsText.events.onInputDown.add(() => {
      window.open(
        'http://www.hkqr.gov.hk/HKQRPRD/web/hkqr-tc/search/qr-search/',
        '_blank'
      )
    })
    detailsText.anchor.setTo(0.5)
    detailsText.resize = (width, height) => {
      const scale = height * 1 * this.textScaleFactor
      detailsText.scale.x = scale
      detailsText.scale.y = scale

      detailsText.x = this.world.centerX + width * 0.1
      detailsText.y = height * 0.55
    }
    detailsText.resize(this.world.width, this.world.height)
    // scaleSprite(detailsText, this.game.width, this.game.height / 3, 50, 1)

    this.add.existing(detailsText)
    this.currentSpirtes.push(detailsText)

    // back button
    let BackButton = new CourseImage({
      game: this.game,
      asset: 'againButton'
    })
    BackButton.inputEnabled = true
    BackButton.input.useHandCursor = true
    BackButton.events.onInputDown.add(() => {
      location.reload()
    })
    BackButton.anchor.setTo(0.5)
    BackButton.resize = (width, height) => {
      const scale = height * 0.17 * this.textScaleFactor
      BackButton.scale.x = scale
      BackButton.scale.y = scale

      BackButton.x = this.world.width * 0.7
      BackButton.y = height * 0.72
    }
    BackButton.resize(this.world.width, this.world.height)
    scaleSprite(BackButton, this.game.width, this.game.height / 3, 0, 0.17)
    BackButton.scaleX = 0.17
    BackButton.scaleY = 0.17
    this.add.existing(BackButton)
    this.currentSpirtes.push(BackButton)

    // FACEBOOK button
    let share = new CourseImage({
      game: this.game,
      asset: 'facebookShare'
    })
    share.inputEnabled = true
    share.input.useHandCursor = true
    share.events.onInputDown.add(() => {
      window.open(
        'http://www.facebook.com/sharer.php?u=' + window.location.href,
        '_blank'
      )
    })
    share.anchor.setTo(0.5)
    share.resize = (width, height) => {
      const scale = height * 0.7 * this.textScaleFactor
      share.scale.x = scale
      share.scale.y = scale
      share.x = this.world.width * 0.52
      share.y = height * 0.72
    }
    share.resize(this.world.width, this.world.height)
    scaleSprite(share, this.game.width / 4, this.game.height / 4, 0, 0.7)
    share.scaleX = 0.4
    share.scaleY = 0.4
    this.add.existing(share)
    this.currentSpirtes.push(share)
   
  }

  displayStep (step) {
    console.log(`Current Step: ${step}`)

    let q = this.questions[step]
    let title = q.qtext
    this.box = new Box({
      game: this.game,
      // x: this.world.width * 0.5,
      // y: this.world.y * 0.7,
      asset: 'box'
    })
    this.box.anchor.setTo(0.5, 0)
    this.box.resize = (width, height) => {
      this.box.x = this.world.centerX + width * 0.1
      this.box.y = this.world.height * 0.05
    }
    this.box.resize(this.world.width, this.world.height)
    this.box.scaleX = 1.6
    this.box.scaleY = 2.5
    // if (step === 3) {
    //   this.box.scaleX = 1.6
    //   this.box.scaleY = 2
    // }
    scaleSpriteXY(
      this.box,
      this.game.width,
      this.game.height / 3,
      50,
      this.box.scaleX,
      this.box.scaleY
    )
  
    this.add.existing(this.box)
    this.currentSpirtes.push(this.box)

    // Create Title
    let titleText = new TitleText({
      game: this.game,
      // x: this.world.width * 0.7,
      // y: this.world.height * 0.08,
      text: title
    })
    let titleX = 0.4
    let titleY = 0.1
    // if (step === 1) {
    //   titleX = 0.4
    //   titleY = 0.08
    // } else if (step === 2) {
    //   titleX = 0.4
    //   titleY = 0.08
    // } else if (step === 3) {
    //   titleX = 0.4
    //   titleY = 0.08
    // }
    // Add resize() handler
    titleText.anchor.setTo(0.5)
    titleText.resize = (width, height) => {
      // Scale
      // titleText.style.font = `bold ${Math.ceil(
      //   this.world.height / 10
      // )}em "Microsoft JhengHei","WenQuanYi Zen Hei","LiHei Pro"`
      const scale = height * this.textScaleFactor
      titleText.scale.x = scale
      titleText.scale.y = scale

      // Position
      titleText.x = this.world.centerX + width * 0.1
      titleText.y = height * titleY
    }
    titleText.resize(this.world.width, this.world.height)
    console.log(titleText)
    this.add.existing(titleText) // Add to Game
    this.currentSpirtes.push(titleText) // Save to Array
    // scaleSprite(titleText, this.game.width, this.game.height, 0, 1)

    // Tween (Effect)
    titleText.y = titleText.y - 50
    this.add
      .tween(titleText)
      .to({ y: titleText.y + 50 }, 1200, Phaser.Easing.Bounce.Out, true)

    if (step === 1) {
      this.addHobbyOptions()
    } else if (step === 2) {
      this.addCourseOptions()
    } else if (step === 3) {
      this.addCourseDetails()
    }
  }
}
