import Phaser from 'phaser'
import WebFont from 'webfontloader'

export default class extends Phaser.State {
  init () {
    this.stage.backgroundColor = '#EDEEC9'
    this.fontsReady = false
    this.fontsLoaded = this.fontsLoaded.bind(this)

    // Continue execute when out of focus
    this.stage.disableVisibilityChange = true
    // Scale
    this.scale.scaleMode = Phaser.ScaleManager.RESIZE
    if (this.game.device.desktop) {
      // this.scale.scaleMode = Phaser.ScaleManager.RESIZE
      // this.scale.setMinMax(480, 320)
      // this.scale.pageAlignHorizontally = true
      // this.scale.pageAlignVertically = true
    } else {
      // this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
      // this.scale.setMinMax(480, 320)
      // this.scale.pageAlignHorizontally = true
      // this.scale.pageAlignVertically = true
      // this.scale.forceOrientation(true, false)
    }
    this.scale.refresh()
  }

  preload () {
    WebFont.load({
      google: {
        families: ['Bangers'] // 'Material Icons'
      },
      active: this.fontsLoaded
    })
    this.load.audio('music', [
      'assets/audio/newbgmusic.mp3',
      'assets/audio/newbgmusic.ogg'
    ])
    this.load.audio('buttonClickSound', 'assets/audio/p-ping.mp3')
    this.load.audio('pageClickSound', 'assets/audio/squit.mp3')
    this.load.audio('characterSound', 'assets/audio/charactor.mp3')

    this.load.image('choose-01.png', 'assets/images/choose_1.png')
    this.load.image('choose-02.png', 'assets/images/choose_2.png')
    this.load.image('choose-03.png', 'assets/images/choose_3.png')
    this.load.image('choose-04.png', 'assets/images/choose_4.png')
    this.load.image('choose-05.png', 'assets/images/choose_5.png')
    this.load.image('choose-06.png', 'assets/images/choose_6.png')
    this.load.image('choose-07.png', 'assets/images/choose_7.png')
    this.load.image('choose-08.png', 'assets/images/choose_8.png')
    this.load.image('choose-09.png', 'assets/images/choose_9.png')
    this.load.image('choose-10.png', 'assets/images/choose_10.png')
    this.load.image('choose-11.png', 'assets/images/choose_11.png')

    // this.load.audio('mouseOver', 'assets/audio/steps1.mp3')
    this.loadingText = this.add.text(
      this.world.centerX,
      this.world.centerY,
      'Loading...',
      {
        font: '36px Arial',
        fill: '#dddddd',
        align: 'center'
      }
    )
    this.loadingText.anchor.setTo(0.5, 0.5)

    this.load.image('modeLandscape', './assets/images/mode-landscape.png')

    this.loaded = false
    this.load.onLoadComplete.add(() => {
      this.loaded = true
    }, this)
  }

  create () {
    this.landscapeImage = new Phaser.Sprite(
      this.game,
      this.world.centerX,
      this.world.centerY,
      'modeLandscape'
    )
    this.landscapeImage.anchor.setTo(0.5)
    this.landscapeImage.visible = true
    let scale = this.world.width / 320 * 0.6
    this.landscapeImage.scale.x = scale
    this.landscapeImage.scale.y = scale
    this.add.existing(this.landscapeImage)
  }

  render () {
    if (this.fontsReady) {
      // Remove loadingText
      this.loadingText.destroy()

      // Show
      this.landscapeImage.visible = true
    }
    this.startGame()
  }

  resize (width, height) {
    // this.startGame()
  }

  startGame () {
    // Start Game iff Landscape
    if (this.scale.isGameLandscape && this.loaded) {
      this.state.start('Splash') // Splash State
      // this.state.start('Game') // Game State
    }
  }

  fontsLoaded () {
    this.fontsReady = true
  }
}
