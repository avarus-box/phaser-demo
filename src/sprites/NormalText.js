import Phaser from 'phaser'

// http://phaser.io/docs/2.6.2/Phaser.Text.html
export default class extends Phaser.Text {
  constructor ({ game, x, y, text }) {
    super(game, x, y, text, null)
    // this.text = text
    this.anchor.setTo(0.5)
    this.smooth = true
    this.style.font = `bold 28pt "WenQuanYi Zen Hei","LiHei Pro","Microsoft JhengHei"`
    this.style.wordWrap = true
    this.style.wordWrapWidth = 300
    this.fill = '#535353'
    // this.setShadow(0, 0, 'rgba(0, 0, 0, 0.5)', 0)
    //  Apply the shadow to the Stroke only
    this.setShadow(1, 1, '#bababa', 1, true, false)

    // this.inputEnabled = true
    // this.events.onInputOver.add(this.over, this)
    // this.events.onInputOut.add(this.out, this)

    // this.events.onInputUp.add(this.up, this)
  }

  over () {
    // console.log('over')
    this.fill = '#ff7f50'
  }

  out () {
    // console.log('out')
    this.fill = '#535353'
  }

  // up () {
  //   this.fill = 'white'
  // }
}
