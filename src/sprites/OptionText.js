import Phaser from 'phaser'

// http://phaser.io/docs/2.6.2/Phaser.Text.html
export default class extends Phaser.Text {
  constructor ({ game, x, y, text }) {
    super(game, x, y, text, null)
    // this.text = text
    this.anchor.setTo(0)
    this.smoothed = true
    this.style.font = `bold 30pt "WenQuanYi Zen Hei","LiHei Pro","Microsoft JhengHei"`
    this.fill = '#535353'
    // this.setShadow(0, 0, 'rgba(0, 0, 0, 0.5)', 0)
    // this.stroke = '#ffd39b'
    // this.strokeThickness = 12
    //  Apply the shadow to the Stroke only
    this.setShadow(2, 2, '#bababa', 2, true, false)

    this.inputEnabled = true
    this.input.useHandCursor = true
    this.events.onInputOver.add(this.over, this)
    this.events.onInputOut.add(this.out, this)

    // this.events.onInputUp.add(this.up, this)
  }

  over () {
    // console.log('over')
    this.fill = '#ff7f50'
  }

  out () {
    // console.log('out')
    this.fill = '#535353'
  }

  // up () {
  //   this.fill = 'white'
  // }
}