import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, relativeX, relativeY, asset }) {
    super(game, x, y, asset)
    // this.anchor.setTo(0.5)

    this.relativeX = relativeX
    this.relativeY = relativeY
    this.smoothed = false
  }

  update () {
    // this.angle += 1
  }

  resize (width, height) {
    // console.log(`Boy::resize(${width}, ${height})`)
    this.x = width * this.relativeX
    this.y = height * this.relativeY
  }
}
