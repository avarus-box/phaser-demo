import Phaser from 'phaser'

// http://phaser.io/docs/2.6.2/Phaser.Text.html
export default class extends Phaser.Text {
  constructor ({ game, x, y, text }) {
    super(game, x, y, text, null)

    // this.text = text
    // Set center point
    // this.anchor.setTo(0.5)
    this.smooth = true
    this.style.font = `bold 40pt "Microsoft JhengHei","WenQuanYi Zen Hei","LiHei Pro"`
    this.style.fontWeight = 800
    this.fill = '#535353'
    // this.setShadow(2, 2, 'rgba(0, 0, 0, 0.5)', 0)

    // this.stroke = '#7ac5cd'
    // this.strokeThickness = 12
    //  Apply the shadow to the Stroke only
    this.setShadow(2, 2, '#bababa', 2, true, false)
    // this.inputEnabled = true
    // this.events.onInputOver.add(this.over, this)
    // this.events.onInputOut.add(this.out, this)

    // this.events.onInputUp.add(this.up, this)
  }

  // resize (width, height) {
  //   // console.log(`Boy::resize(${width}, ${height})`)
  //   // this.x = width * this.relativeX
  //   // this.y = height * this.relativeY
  //   if (this.resizeCallback !== null) {
  //     this.resizeCallback(width, height)
  //   }
  // }

  over () {
    // console.log('over')
    // this.fill = 'blue'
  }

  out () {
    // console.log('out')
    // this.fill = 'white'
  }

  // up () {
  //   this.fill = 'white'
  // }
}