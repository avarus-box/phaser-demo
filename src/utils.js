import Phaser from 'phaser'

const scaleSprite = (
  sprite,
  availableSpaceWidth,
  availableSpaceHeight,
  padding,
  scaleMultiplier
) => {
  const sWidth = sprite._frame ? sprite._frame.width : sprite.width
  const sHeight = sprite._frame ? sprite._frame.height : sprite.height

  // if (availableSpaceWidth > availableSpaceHeight) {
  //   if (availableSpaceWidth < 768){
  //     availableSpaceWidth = availableSpaceWidth * 1.2
  //     availableSpaceHeight = availableSpaceHeight * 1.2
  //   }
  // }
  let scale = getSpriteScale(
    sprite._frame.width,
    sprite._frame.height,
    availableSpaceWidth,
    availableSpaceHeight,
    padding
  )

  // if (__DEV__) {
  // let spriteSize = `${sWidth}*${sHeight}`
  // let type = getSpriteType(sprite)
  // // console.log(sprite)
  // console.log(
  //   `${type} "${sprite.text ||
  //      sprite.name}" (${spriteSize}) | Scale: ${scale} * ${scaleMultiplier}`
  // )
  // }

  sprite.scale.x = scale * scaleMultiplier
  sprite.scale.y = scale * scaleMultiplier
}

const scaleSpriteXY = (
  sprite,
  availableSpaceWidth,
  availableSpaceHeight,
  padding,
  scaleMultiplierX,
  scaleMultiplierY
) => {
  const sWidth = sprite._frame ? sprite._frame.width : sprite.width
  const sHeight = sprite._frame ? sprite._frame.height : sprite.height

  // if (availableSpaceWidth > availableSpaceHeight) {
  //   if (availableSpaceWidth < 768){
  //     availableSpaceWidth = availableSpaceWidth * 1.2
  //     availableSpaceHeight = availableSpaceHeight * 1.2
  //   }
  // }
  let scale = getSpriteScale(
    sWidth,
    sHeight,
    availableSpaceWidth,
    availableSpaceHeight,
    padding
  )

  if (__DEV__) {
    // let spriteSize = `${sWidth}*${sHeight}`
    // let type = getSpriteType(sprite)
    // console.log(
    //   `${type} "${sprite.text ||
    //     sprite.name}" (${spriteSize})} * (${scaleMultiplierX}, ${scaleMultiplierY})`
    // )
  }

  sprite.scale.x = scale * scaleMultiplierX
  sprite.scale.y = scale * scaleMultiplierY
}

const getSpriteScale = (
  spriteWidth,
  spriteHeight,
  availableSpaceWidth,
  availableSpaceHeight,
  minPadding
) => {
  let ratio = 1
  let currentDevicePixelRatio = window.devicePixelRatio
  // Sprite needs to fit in either width or height
  // if (availableSpaceWidth > availableSpaceHeight) {
  //   // orientation = 'landscape'
  //   if (availableSpaceWidth < 768){
  //     availableSpaceWidth = availableSpaceWidth * 1.2
  //     availableSpaceHeight = availableSpaceHeight * 1.2
  //   }
  // }
  let widthRatio =
    (spriteWidth * currentDevicePixelRatio + 2 * minPadding) /
    availableSpaceWidth
  let heightRatio =
    (spriteHeight * currentDevicePixelRatio + 2 * minPadding) /
    availableSpaceHeight
  // console.log('widthRatio: ' + widthRatio)
  // console.log('heightRatio: ' + heightRatio)
  console.log('availableSpaceWidth: ' + availableSpaceWidth)
  console.log('availableSpaceHeight: ' + availableSpaceHeight)
  if (widthRatio > 1 || heightRatio > 1) {
    ratio = 1 / Math.max(widthRatio, heightRatio)
  }
  // console.log('ratio: ' + ratio)
  return ratio * currentDevicePixelRatio
}

const destroySpirtes = spirtes => {
  // Destroy Current Spirte
  for (let i = 0; i < spirtes.length; i += 1) {
    spirtes[i].destroy()
    // this.destroy(this.currentSpirte[i]);
  }
}

const centerGameObjects = objects => {
  objects.forEach(function (object) {
    object.anchor.setTo(0.5)
  })
}

const isText = s => s instanceof Phaser.Text
const isGroup = s => s instanceof Phaser.Group

const getSpriteType = s => {
  if (isGroup(s)) {
    return 'group'
  } else if (isText(s)) {
    return 'text'
  } else {
    return s.constructor.name
  }
}

export default {
  isText,
  isGroup
}
export {
  getSpriteType,
  scaleSpriteXY,
  scaleSprite,
  getSpriteScale,
  destroySpirtes,
  isText
}
